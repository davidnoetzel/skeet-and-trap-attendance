The Skeet & Trap Attendance System is a script that makes it easier to know
who is and isn't coming to practice and how many drivers are needed to carry
people to the fields. Using Google Forms to collect responses, it automatically
notifies drivers whether or not they need to drive and sends a summary of the
attendance to the captains and the coaches.



INSTRUCTIONS:
1.  Log into your Yale email and access Drive.
1a. (Optional) I recommend creating a new folder to hold all of these files,
    since the script will create a lot of spreadsheets and forms.
2.  Create a new spreadsheet and open it. Give the file a relevant name so that
    you will not be tempted to delete it.
3.  In the menu click Tools > Script editor...
4.  In the new tab, under "Create script for", select "Blank Project".
5.  Delete the existing text.
6.  Copy and paste the entire script. Save the file. Any project name will do.
7.  Refresh or open again the spreadsheet you created.
8.  From the Skeet & Trap menu, enter the information about significant dates,
    the coaches and team leadership, and the whole team.
8a. The first time you try and interact with the script, it will prompt you for
    authorization. Click "Continue" and then "Accept". An explanation of the
    permissions can be found in the PERMISSIONS section below.
9.  Once all information has been entered, click "Initialize calendar".
10. Take it easy. The script will take care of almost everything else.



NOTES:
- If a button does not immediately respond, just wait. Double-clicking buttons
  can cause unexpected results.
- The links to the forms for practice attendance need to be emailed out
  manually; the email with that link is a good opportunity to make announcements
  to the team and give them practice-specific information. To get the link to a
  form, open the form in Drive and click "View live form". The newly opened tab
  is the web address that should be sent out.
- If you need to add or remove practices (for example, a new weekend practice or
  a regular practice that is cancelled due to weather), or add or remove team
  members (for example, a person that joins second semester or a person who is
  going abroad for a semester), click the appropriate menu items.



PERMISSIONS:
View and manage your forms in Google Drive
Each team member fills out a form detailing whether he will be coming to
practice and if he will be driving.

View and manage your spreadsheets in Google Drive
Data about each practice (the results from the form you send out every week) is
stored in spreadsheets.

View and manage data associated with the application
Properties of the script are used to store information about the next practice.

Send email as you
The script sends out emails to: (i) remind those who have not yet filled out the
form to do so, (ii) notify the drivers required for practice and (iii) to inform
the coaches and the team leadership who is not coming to practice and why, and
who did not fill out the form.

Allow this application to run when you are not present
The notification emails are sent out at specific times, so the script will run
autonomously.