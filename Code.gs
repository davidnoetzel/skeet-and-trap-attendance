/**
 * @file Handles all functions related to practice attendance and related
 *       notifications for the Yale Skeet & Trap Team.
 * @author David Noetzel
 */

/*  Skeet & Trap Attendance System
 *  Copyright (C) 2014 David Noetzel
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



/**
 * An object that stores the names and indices of sheets used to store data
 * @type {Object}
 * @const
 */
var SHEET_INDEX = {PRACTICES : {name : 'Practice Dates and Keys', index : 0},
                   BREAKS : {name : 'Break Dates', index : 1},
                   LEADERSHIP : {name : 'Leadership Information', index : 2},
                   TEAM : {name : 'Team Information', index : 3}};



/**
 * Asks for confirmation before creating the practice schedule.
 * @param {String} semester The semester to be initialized
 */
function openCalendarInit() {
  var promptString = 'Are you sure you want to create the practice schedule '+
                     'for the year? This action will delete any other triggers'+
                     ' that control the forms and email notifications.';
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert(promptString, ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    deleteTriggers();
    createCalendar();
    confirmComplete();
  }

  return;
}



/**
 * Creates the required spreadsheets, forms and triggers for the given semester.
 * @param {String} semester The semester to be initialized
 */
function createCalendar() {
  var data = readFromSheet(SHEET_INDEX.BREAKS);
  var d;
  var firstSemesterStart;
  var firstSemesterEnd;
  var secondSemesterStart;
  var secondSemesterEnd;
  var breakStartIndex;

  for (var i = 0; i < data.length; i++) {
    //Select the start dates for each semester
    if (data[i][0] == 'firstSemesterStartDW') {
      firstSemesterStart = new Date(data[i][1]);
      firstSemesterEnd = new Date(data[i+1][1]);
      var scriptProperties = PropertiesService.getScriptProperties();
      scriptProperties.setProperty('nextPractice',
                                   firstSemesterStart.toString());
    }
    else if (data[i][0] == 'secondSemesterStartDW') {
      secondSemesterStart = new Date(data[i][1]);
      secondSemesterEnd = new Date(data[i+1][1]);
      //Breaks should always be listed after the end of the second semester
      breakStartIndex = i+2;
    }
  }

  d = firstSemesterStart;

  //Create practices every seven days after the first practice
  while (d <= firstSemesterEnd) {
    if (!hasConflicts(d, data, breakStartIndex)) {
      createPractice(d);
    }

    d.setDate(d.getDate() + 7);
  }

  d = secondSemesterStart;

  while (d <= secondSemesterEnd) {
    if (!hasConflicts(d, data, breakStartIndex)) {
      createPractice(d);
    }

    d.setDate(d.getDate() + 7);
  }

  setNextTriggers();

  return;
}



/**
 * Creates the spreadsheet, form and trigger for the given date.
 * @param {Date} d The date when a practice will be held
 */
function createPractice(d) {
  var meta = SpreadsheetApp.getActiveSpreadsheet();
  var sheetId = SHEET_INDEX.PRACTICES;
  var metaSheet = meta.getSheetByName(sheetId.name);

  //Specified sheet does not yet exist
  if (!metaSheet) {
    metaSheet = meta.insertSheet(sheetId.name, sheetId.index);
  }

  var ss = SpreadsheetApp.create('Practice Attendance for ' +
                                 simpleDate(d));
  var form = FormApp.create('Practice Attendance for ' + simpleDate(d));

  var attendance = form.addMultipleChoiceItem();
  attendance.setTitle('Are you coming to practice this week?');
  attendance.setChoices([
    attendance.createChoice('Yes'),
    attendance.createChoice('No')
    ]);
  form.addParagraphTextItem().setTitle('If you answered "No," please state ' +
                                       'your reason for missing practice.');
  var driving = form.addMultipleChoiceItem();
  driving.setTitle('Are you driving this practice?');
  driving.setChoices([
    driving.createChoice('Yes'),
    driving.createChoice('No')
    ]);
  attendance.setRequired(true);
  driving.setRequired(true);

  form.setRequireLogin(true);
  form.setCollectEmail(true);
  form.setAllowResponseEdits(true);
  form.setShowLinkToRespondAgain(false);
  form.setDestination(FormApp.DestinationType.SPREADSHEET, ss.getId());
  ss.deleteSheet(ss.getSheetByName('Sheet1'));

  var leadershipEmails = getLeadershipEmails();
  ss.addEditors(leadershipEmails);
  form.addEditors(leadershipEmails);

/*  var debugTime = new Date();
  debugTime.setMinutes(debugTime.getMinutes() + 3);
  setTrigger(debugTime);
  var dateTime = debugTime;*/

  var range = metaSheet.getRange(metaSheet.getLastRow()+1, 1, 1, 3);
  range.setValues([[d.toString(), ss.getId(), form.getId()]]);

  return;
}



/**
 * Opens a window to record important dates for setting up practices.
 * @return {UiInstance} The created app with fields for dates
 */
function openDateInit() {
  var app = UiApp.createApplication().setTitle('Enter Dates').setHeight(450);
  var grid = app.createGrid(14, 2);

  var firstSemesterStartDW = app.createDateBox().setId('firstSemesterStartDW');
  var firstSemesterEndDW = app.createDateBox().setId('firstSemesterEndDW');
  var secondSemesterStartDW = app.createDateBox().setId('secondSemesterStartDW');
  var secondSemesterEndDW = app.createDateBox().setId('secondSemesterEndDW');
  var fallBreakStartDW = app.createDateBox().setId('fallBreakStartDW');
  var fallBreakEndDW = app.createDateBox().setId('fallBreakEndDW');
  var thanksgivingBreakStartDW = app.createDateBox().setId('thanksgivingBreakStartDW');
  var thanksgivingBreakEndDW = app.createDateBox().setId('thanksgivingBreakEndDW');
  var springBreakStartDW = app.createDateBox().setId('springBreakStartDW');
  var springBreakEndDW = app.createDateBox().setId('springBreakEndDW');
  var virginiaStartDW = app.createDateBox().setId('virginiaStartDW');
  var virginiaEndDW = app.createDateBox().setId('virginiaEndDW');
  var nationalsStartDW  = app.createDateBox().setId('nationalsStartDW');
  var nationalsEndDW = app.createDateBox().setId('nationalsEndDW');

  grid.setWidget(0, 0, app.createLabel('First semester first practice'));
  grid.setWidget(0, 1, firstSemesterStartDW);
  grid.setWidget(1, 0, app.createLabel('First semester end'));
  grid.setWidget(1, 1, firstSemesterEndDW);
  grid.setWidget(2, 0, app.createLabel('Second semester first practice'));
  grid.setWidget(2, 1, secondSemesterStartDW);
  grid.setWidget(3, 0, app.createLabel('Second semester end'));
  grid.setWidget(3, 1, secondSemesterEndDW);
  grid.setWidget(4, 0, app.createLabel('Fall break start'));
  grid.setWidget(4, 1, fallBreakStartDW);
  grid.setWidget(5, 0, app.createLabel('Fall break end'));
  grid.setWidget(5, 1, fallBreakEndDW);
  grid.setWidget(6, 0, app.createLabel('Thanksgiving break start'));
  grid.setWidget(6, 1, thanksgivingBreakStartDW);
  grid.setWidget(7, 0, app.createLabel('Thanksgiving break end'));
  grid.setWidget(7, 1, thanksgivingBreakEndDW);
  grid.setWidget(8, 0, app.createLabel('Spring break start'));
  grid.setWidget(8, 1, springBreakStartDW);
  grid.setWidget(9, 0, app.createLabel('Spring break end'));
  grid.setWidget(9, 1, springBreakEndDW);
  grid.setWidget(10, 0, app.createLabel('Virginia start'));
  grid.setWidget(10, 1, virginiaStartDW);
  grid.setWidget(11, 0, app.createLabel('Virginia end'));
  grid.setWidget(11, 1, virginiaEndDW);
  grid.setWidget(12, 0, app.createLabel('Nationals start'));
  grid.setWidget(12, 1, nationalsStartDW);
  grid.setWidget(13, 0, app.createLabel('Nationals end'));
  grid.setWidget(13, 1, nationalsEndDW);

  var submitButton = app.createButton('Submit');
  var submitDatesHandler = app.createServerHandler('submitDates');
  submitDatesHandler.addCallbackElement(grid);
  submitButton.addClickHandler(submitDatesHandler);

  var cancelButton = app.createButton('Cancel');
  var cancelHandler = app.createServerHandler('cancel');
  cancelButton.addClickHandler(cancelHandler);

  var panel = app.createVerticalPanel();
  panel.add(grid);
  panel.add(cancelButton);
  panel.add(submitButton);
  app.add(panel);

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.show(app);

  return app;
}



/**
 * Records the date ranges when we do not have practice.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function submitDates(e) {
  var results = [];
  var params = ['firstSemesterStartDW', 'firstSemesterEndDW',
                'secondSemesterStartDW', 'secondSemesterEndDW',
                'fallBreakStartDW', 'fallBreakEndDW',
                'thanksgivingBreakStartDW', 'thanksgivingBreakEndDW',
                'springBreakStartDW', 'springBreakEndDW',
                'virginiaStartDW', 'virginiaEndDW',
                'nationalsStartDW', 'nationalsEndDW'
               ];

  //Associate date names with strings representing the dates
  for (var i = 0; i < params.length; i++) {
    results.push([params[i], e.parameter[params[i]].toString()]);
  }

  if (confirmMissing(results)) {
    writeToSheet(SHEET_INDEX.BREAKS, results);
    confirmComplete();
  }

  var app = UiApp.getActiveApplication();
  app.close();

  return app;
}



/**
 * Closes the current window.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function cancel(e) {
  var app = UiApp.getActiveApplication();
  app.close();

  return app;
}



/**
 * Opens a window to record names and email addresses and for the coaches and
 * team leaders.
 */
function leadershipInit() {
  var instructionString = 'Enter the names and email addresses of the coaches '+
                          'and the relevant team leaders that should receive '+
                          'email notifications about practice attendance. '+
                          'There is no need to enter driver information. '+
                          'Do not double-click the + or - buttons.';

  openMailInit('Leadership', instructionString);

  return;
}



/**
 * Opens a window to record names, email addresses and driver information for
 * team members.
 */
function teamInit() {
  var instructionString = 'Only enter car capacity and willingness for known';
                          ' drivers. Do not double-click the + or - buttons.';

  openMailInit('Team', instructionString);

  return;
}



/**
 * Opens a window to record names, email addresses and driver information.
 * @param {String} title The string which determines what info to collect
 * @param {String} instructionString A set of instructions for the user
 * @return {UiInstance} The created app
 */
function openMailInit(title, instructionString) {
  var app = UiApp.createApplication();
  app.setTitle('Enter '+title+' Information').setWidth(700).setHeight(400);

  var panel = app.createVerticalPanel();
  var instructions = app.createLabel(instructionString);
  //Tag will count the number of members
  var table = app.createFlexTable().setId('table').setTag('0');
  //Write the header for the table
  var headerArray = ['First Name', 'Last Name', 'Email', 'Car Capacity',
                     'Driver Willingness'];
  for (var i = 0; i < headerArray.length; i++) {
    table.setWidget(0, i, app.createLabel(headerArray[i]));
  }

  //Add the first row of form elelments to input Member information
  addMemberRow(app);

  //Add a button to submit the info
  var submitButton = app.createButton('Submit');
  var submitInfoHandler = app.createServerHandler('submit'+title+'Info');
  submitInfoHandler.addCallbackElement(panel);
  submitButton.addMouseUpHandler(submitInfoHandler);

  var cancelButton = app.createButton('Cancel');
  var cancelHandler = app.createServerHandler('cancel');
  cancelButton.addClickHandler(cancelHandler);

  var scroll = app.createScrollPanel().setPixelSize(700, 400)
  panel.add(instructions)
    .add(table)
    .add(cancelButton)
    .add(submitButton);
  scroll.add(panel);
  app.add(scroll);

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.show(app);

  return app;
}



/**
 * Draws a new row in the information window.
 * @param {UiInstance} app The app to which rows will be added
 */
function addMemberRow(app) {
  var table = app.getElementById('table');
  var tag = parseInt(table.getTag());
  var numRows = tag+1;

  //Remove add/remove buttons
  if (numRows > 1) {
    table.removeCell(numRows-1, 6);
    table.removeCell(numRows-1, 5);
  }

  var firstNameWidget = app.createTextBox().setId('firstName'+numRows)
                        .setName('firstName'+numRows);
  var lastNameWidget = app.createTextBox().setId('lastName'+numRows)
                       .setName('lastName'+numRows);
  var emailAddressWidget = app.createTextBox().setId('emailAddress'+numRows)
                           .setName('emailAddress'+numRows);
  var capacityWidget = app.createListBox().setId('capacity'+numRows)
                       .setName('capacity'+numRows);
  var willingnessWidget = app.createListBox().setId('willingness'+numRows)
                          .setName('willingness'+numRows);

  capacityWidget.addItem('');

  for (var i = 1; i <= 7; i++) {
    capacityWidget.addItem(''+i);
  }

  willingnessWidget.addItem('');

  for (var i = 1; i <= 10; i++) {
    willingnessWidget.addItem(''+i);
  }

  table.setWidget(numRows, 0, firstNameWidget);
  table.setWidget(numRows, 1, lastNameWidget);
  table.setWidget(numRows, 2, emailAddressWidget);
  table.setWidget(numRows, 3, capacityWidget);
  table.setWidget(numRows, 4, willingnessWidget);
  table.setTag(numRows.toString());

  addButtons(app);

  return;
}



/**
 * Draws the add and remove buttons in the information window.
 * @param {UiInstance} app The app to which buttons are being added
 */
function addButtons(app) {
  var table = app.getElementById('table');
  var numRows = parseInt(table.getTag());

  //Create handler to add/remove row
  var addRemoveRowHandler = app.createServerHandler('addRemoveRow');
  addRemoveRowHandler.addCallbackElement(table);

  //Remove row button and handler
  var removeRowBtn = app.createButton('-').setId('removeOne')
                     .setTitle('Remove row');
  table.setWidget(numRows, 5, removeRowBtn);
  removeRowBtn.addMouseUpHandler(addRemoveRowHandler);

  //Add row button and handler
  var addRowBtn = app.createButton('+').setId('addOne').setTitle('Add row');
  table.setWidget(numRows, 6, addRowBtn);
  addRowBtn.addMouseUpHandler(addRemoveRowHandler);

  return;
}



/**
 * Adds or removes a row in the information window.
 * @param {Object} e The parameters passed by the server handler
 */
function addRemoveRow(e) {
  var app = UiApp.getActiveApplication();
  var table = app.getElementById('table');
  var tag = parseInt(e.parameter.table_tag);
  var source = e.parameter.source;

  if (source == 'addOne') {
    table.setTag(tag.toString());
    addMemberRow(app);
  }
  else if (source == 'removeOne') {
    if (tag > 1) {
      //Decrement the tag by one
      var numRows = tag-1;
      table.removeRow(tag);
      //Set the new tag of the table
      table.setTag(numRows.toString());
      //Add buttons in previous row
      addButtons(app);
    }
  }

  return app;
}



/**
 * Writes the names and email addresses of coaches and leadership to a
 * spreadsheet.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function submitLeadershipInfo(e) {
  return submitInfo(e, SHEET_INDEX.LEADERSHIP);
}



/**
 * Returns the email addresses of the coaches and team leadership.
 * @return {String[]} An array of email addresses
 */
function getLeadershipEmails() {
  var data = readFromSheet(SHEET_INDEX.LEADERSHIP);

  return getCol(data, 2);
}



/**
 * Writes the names, email addresses and driver information of team members to
 * a spreadsheet.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function submitTeamInfo(e) {
  return submitInfo(e, SHEET_INDEX.TEAM);
}



/**
 * Returns the email addresses of all team members.
 * @return {String[]} An array of email addresses
 */
function getTeamEmails() {
  var data = readFromSheet(SHEET_INDEX.TEAM);

  return getCol(data, 2);
}



/**
 * Returns the full names of all team members.
 * @return {String[]} An array of names
 */
function getTeamNames() {
  var data = readFromSheet(SHEET_INDEX.TEAM);
  var names = [];

  for (var i = 0; i < data.length; i++) {
    names.push(data[i][0]+' '+data[i][1]);
  }

  return names;
}



/**
 * Writes personal information to a spreadsheet.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function submitInfo(e, sheetId) {
  var results = [];
  var numMembers = parseInt(e.parameter.table_tag);

  //Create array from member info
  for (var i = 1; i <= numMembers; i++) {
    var member = [];
    member.push(e.parameter['firstName'+i]);
    member.push(e.parameter['lastName'+i]);
    member.push(e.parameter['emailAddress'+i].toLowerCase());
    member.push(e.parameter['capacity'+i]);
    member.push(e.parameter['willingness'+i]);
    results.push(member);
  }

  //Do not check driver columns for missing info, since not every person drives
  if (confirmMissing(results, 3)) {
    writeToSheet(sheetId, results);
    confirmComplete();
  }

  var app = UiApp.getActiveApplication();
  app.close();

  return app;
}



/**
 * Returns true if the target date falls during any break.
 * @param {Date} targetDate The date to be tested for conflicts
 * @param {Object[][]} data The information read from the Break Dates sheet
 * @param {Integer} start The row where the first date to check is located
 * @return {Boolean} Whether the date has a conflict
 */
function hasConflicts(targetDate, data, start) {
  for (var i = start; i < data.length; i +=2 ) {
    var startDate = new Date(data[i][1]);
    var endDate = new Date(data[i+1][1]);

    if (overlaps(targetDate, startDate, endDate)) {
      return true;
    }
  }

  return false;
}



/**
 * Returns true if the first date falls between the second and third dates.
 * @param {Date} targetDate The date to be tested for conflicts
 * @param {Date} startDate The start of the range in question
 * @param {Date} endDate The end of the range in question
 * @return {Boolean} Whether the target date overlaps with the other two dates
 */
function overlaps(targetDate, startDate, endDate) {
  return startDate <= targetDate && targetDate <= endDate;
}



/**
 * Closes a form for submissions and sends out the notification emails.
 */
function closeForm() {
  var pair = getNextPracticeIDs();
  var ssId = pair[0];
  var formId = pair[1];
  var ss = SpreadsheetApp.openById(ssId);
  var form = FormApp.openById(formId);
  form.setAcceptingResponses(false);
  advancePractice();
  setNextTriggers();
  sendNotification(ssId);

  return;
}



/**
 * Returns the IDs of the spreadsheet and form for the next practice.
 * @return {String[]} An array with relevant document IDs
 */
function getNextPracticeIDs() {
  var scriptProperties = PropertiesService.getScriptProperties();
  var d = new Date(scriptProperties.getProperty('nextPractice'));
  var data = readFromSheet(SHEET_INDEX.PRACTICES);

  for (var i = 0; i < data.length; i++) {
    var searchDate = new Date(data[i][0]);

    if (d.getTime() == searchDate.getTime()) {
      return [data[i][1], data[i][2]];
    }
  }
}



/**
 * Returns the date of the next practice.
 * @return {String} The string of the date of the next practice
 */
function getNextPracticeDate() {
  var scriptProperties = PropertiesService.getScriptProperties();

  return scriptProperties.getProperty('nextPractice');
}



/**
 * Sets the nextPractice property to the date of the given practice.
 * @param {String} dateString The date of the practice being assigned
 */
function setNextPracticeDate(dateString) {
  var scriptProperties = PropertiesService.getScriptProperties();
  scriptProperties.setProperty('nextPractice', dateString);

  return;
}



/**
 * Sets the nextPractice property to the next chronological practice.
 */
function advancePractice() {
  var scriptProperties = PropertiesService.getScriptProperties();
  var d = new Date(scriptProperties.getProperty('nextPractice'));
  var data = readFromSheet(SHEET_INDEX.PRACTICES);

  for (var i = 0; i < data.length-1; i++) {
    var searchDate = new Date(data[i][0]);

    if (d.getTime() == searchDate.getTime()) {
      scriptProperties.setProperty('nextPractice', data[i+1][0])
      break;
    }
  }

  return;
}



/**
 * Sets up a trigger to close a form at specified date and time.
 * @param {Date} dateTime The time and date at which to close the form
 * @param {String} f The function to be run
 * @return {Trigger} The created trigger
 */
function setTrigger(dateTime, f) {
  return ScriptApp.newTrigger(f).timeBased().at(dateTime).create();
}



/**
 * Removes all triggers.
 */
function deleteTriggers() {
  var triggers = ScriptApp.getProjectTriggers();

  for (var i = 0; i < triggers.length; i++) {
    ScriptApp.deleteTrigger(triggers[i]);
  }

  return;
}



/**
 * Returns a copy of the form submission data with no duplicate information; in
 * cases where a user submitted more than once, only the last entry is returned.
 * @param {Object[][]} data The form results to be checked
 * @return {Object[][]} A 2D array with the last submissions from each user
 */
function getUniqueSubmissions(data) {
  var uniqueData = [];
  var duplicate = false;
  var emailAddress;

  //Search every entry against every other currently recorded unique entry
  for (var i = 0; i < data.length; i++) {
    for (var j = 0; j < uniqueData.length; j++) {
      emailAddress = data[i][1].toLowerCase();
      //Emails match, meaning the user has multiple submissions
      if (emailAddress == uniqueData[j][1].toLowerCase()) {
        //Save the most recently seen submission
        uniqueData[j] = data[i];
        duplicate = true;
        break;
      }
    }

    if (!duplicate) {
      uniqueData.push(data[i]);
    }
    else {
      duplicate = false;
    }
  }

  return uniqueData;
}



/**
 * Returns an object containing an array of those attending practice, a 2D array
 * of those absent and their reasons, and an array of the available drivers.
 * @param {Object[][]} uniqueData A 2D array of the unique form submission data
 * @return {Object} An object with those attending and absent, and the drivers
 */
function classifyRespondents(uniqueData) {
  var drivers = getDrivers();
  var attending = [];
  var absent = [];
  var unsortedDrivers = [];
  var emailAddress;

  //First row contains column titles, so start at 1
  for (var i = 1; i < uniqueData.length; i++) {
    emailAddress = uniqueData[i][1].toLowerCase();

    if (uniqueData[i][2] == 'Yes') {
      attending.push(emailAddress);

      //Record attending drivers
      for (var j = 0; j < drivers.length; j++) {
        if (emailAddress == drivers[j][0].toLowerCase() &&
            uniqueData[i][4] == 'Yes') {
          unsortedDrivers.push(drivers[j]);
          break;
        }
      }
    }
    else {
      absent.push([emailAddress, uniqueData[i][3]]);
    }
  }

  var results = {attending : attending,
                 absent : absent,
                 unsortedDrivers : unsortedDrivers};

  return results;
}



/**
 * Returns an array of the email addresses of those who did not fill out the
 * form.
 * @param {Object[][]} uniqueData A 2D array of the unique form submission data
 * @return {String[]} An array of the emails of non-responders
 */
function getNonResponders(uniqueData) {
  var noResponse = [];
  var teamEmails = getTeamEmails();
  var emailAddress;

  for (var i = 0; i < teamEmails.length; i++) {
    var didNotRespond = true;

    //First row contains column titles, so start at 1
    for (var j = 1; j < uniqueData.length; j++) {
      emailAddress = uniqueData[j][1].toLowerCase();

      if (teamEmails[i] == emailAddress) {
        didNotRespond = false;
        break;
      }
    }

    if (didNotRespond) {
      noResponse.push(teamEmails[i]);
    }
  }

  return noResponse;
}



/**
 * Sends requests for a person to drive. Only selects as many drivers as needed
 * or all drivers if there is less capacity than team members attending
 * practice, with the most willing drivers selected first.
 * @param {String[]} unsortedDrivers A 2D array of driver email addresses
 * @return {Integer} The capacity of the drivers' cars
 */
function sendDriverEmails(unsortedDrivers, attending, noResponse) {
  var sortedDrivers = unsortedDrivers.sort(sortMultiDimensional);
  var neededDrivers = [];
  var unneededDrivers = [];
  var haveCapacity = 0;

  for (var i = 0; i < sortedDrivers.length; i++) {
    if (haveCapacity >= (attending.length + noResponse.length)) {
      unneededDrivers.push(sortedDrivers[i]);
    }
    else {
      neededDrivers.push(sortedDrivers[i]);
      haveCapacity += sortedDrivers[i][1];
    }
  }

  for (var i = 0; i < neededDrivers.length; i++) {
    MailApp.sendEmail(neededDrivers[i][0], 'Driving notification for practice',
                      'This is an automated message. Since you indicated that '+
                      'you are willing to drive this practice, you have been '+
                      'selected as a driver this week. If you believe this is '+
                      'a mistake, contact the captains immediately. Thank you '+
                      'for your cooperation.');
  }

  for (var i = 0; i < unneededDrivers.length; i++) {
    MailApp.sendEmail(unneededDrivers[i][0], 'Driving notification for practice',
                      'This is an automated message. Though you indicated '+
                      'that you are willing to drive this practice, you have '+
                      'NOT been selected as a driver this week. If you wish '+
                      'to bring your car regardless, you are welcome to do '+
                      'so.');
  }

  return haveCapacity;
}



/**
 * Generates the body of the notification email sent to the team leadership.
 * @param {Integer} haveCapacity The capacity of the drivers' vehicles
 * @param {Spreadsheet} ss The spreadsheet with form responses
 * @param {String[]} attending An array of the emails of those attending
 * @param {String[][]} absent A 2D array of those absent and their reasons
 * @param {String[]} noResponse An array of the emails of non-responders
 * @return {String} The generated body of the email
 */
function generateBody(haveCapacity, ss, attending, absent, noResponse) {
  var body = 'This is an automated message to inform you about practice '+
             'attendance.\n\n';
  body += 'We know ' + attending.length + ' people are coming to practice ';

  if (noResponse.length == 0) {
    body += 'and everyone responded. ';
  }
  else {
    body += 'and ' + noResponse.length;

    if (noResponse.length == 1) {
      body += ' person did not respond. ';
    }
    else {
      body += ' people did not respond. ';
    }
  }

  body += 'Excluding Coach\'s vehicle, we have capacity for ' + haveCapacity +
          ' people. In other words, ';

  if (attending.length > haveCapacity) {
      body += 'we DO need Coach to take people back to Yale. Sorry about that.';
  }
  else if (noResponse.length != 0 &&
           (attending.length + noResponse.length) > haveCapacity) {
      body += 'due to non-responsiveness, we MAY need Coach to take people '+
              'back to Yale. Sorry about that.';
  }
  else if (noResponse.length != 0 &&
           (attending.length + noResponse.length) <= haveCapacity) {
      body += 'even with the non-responsiveness, we DO NOT need Coach to take '+
              'people back to Yale.';
  }
  else {
    body += 'we DO NOT need Coach to take people back to Yale.';
  }

  if (noResponse.length > 0) {
    body += '\n\nThe following people did not respond:';

    for (var i = 0; i < noResponse.length; i++) {
      body += '\n' + emailToName(noResponse[i]);
    }
  }

  if (absent.length > 0) {
    body += '\n\nThe following people are not attending practice:\n';
    for (var i = 0; i < absent.length; i++) {
      body += emailToName(absent[i][0]);
      body += '\nReason: ' + absent[i][1] + '\n';
    }
  }

  body += '\n\nYou can view more detailed results here:\n' + ss.getUrl();

  return body;
}



/**
 * Sends a summary email to Coach and the captains, highlighting how many people
 * are coming, who did not respond to the survey, who will be absent and if
 * Coach needs to return to campus.
 * @param {String} ssId The ID of the spreadsheet containing attendance info
 */
function sendNotification(ssId) {
  var ss = SpreadsheetApp.openById(ssId);
  var leadershipEmails = getLeadershipEmails();
  var editorString = leadershipEmails.join(', ');

  //Write the unique data to a new sheet in the form results spreadsheet
  var sheet = ss.getSheetByName('Form Responses 1');
  var data = sheet.getDataRange().getValues();
  var uniqueData = getUniqueSubmissions(data);
  var uniqueSheet = ss.insertSheet('Unique Data', 0);
  var r = uniqueSheet.getRange(1, 1, uniqueData.length, uniqueData[0].length);
  r.setValues(uniqueData);
  var range = sheet.getRange('A1:E1');
  range.copyFormatToRange(uniqueSheet, 1, 4, 1, 1);

  var results = classifyRespondents(uniqueData);
  var attending = results.attending;
  var absent = results.absent;
  var unsortedDrivers = results.unsortedDrivers;
  var noResponse = getNonResponders(uniqueData);
  var haveCapacity = sendDriverEmails(unsortedDrivers, attending, noResponse);

/*  var body = "THIS IS ONLY A TEST. This is David Noetzel, testing out the script for practice " +
    "attendance. This is one of three emails, the text of which has the form of the automated " +
    "messages that will be sent out at 6AM on the morning of practices with attendance and " +
    "capacity information. Please send me an email telling me whether or not you received all " +
    "three emails. I apologize for crowding up your inbox, but any good system is tested in "+
    "real-world-like conditions before it is relied upon. The following is what the message "+
    "will look like when the system is in use (without the dummy data, of course):\n\n";
*/
  var body = generateBody(haveCapacity, ss, attending, absent, noResponse);

  MailApp.sendEmail(editorString, ss.getName(), body);

  return;
}



/**
 * Returns driver information
 * @return {Object[]} An array of the drivers' email, capacity and willingness
 */
function getDrivers() {
  var data = readFromSheet(SHEET_INDEX.TEAM);
  var drivers = [];

  for (var i = 0; i < data.length; i++) {
    if (parseInt(data[i][3]) && parseInt(data[i][4])) {
      drivers.push([data[i][2], parseInt(data[i][3]), parseInt(data[i][4])]);
    }
  }

  return drivers;
}



/**
 * Gets a person's name from the Team sheet based on his on email address.
 * @param {String} emailAddress The email address of the person to be found
 * @return {String} The desired name
 */
function emailToName(emailAddress) {
  var data = readFromSheet(SHEET_INDEX.TEAM);

  for (var i = 0; i < data.length; i++) {
    if (emailAddress == data[i][2]) {
      return data[i][0]+' '+data[i][1];
    }
  }

  //Name not found in the team sheet; extract name from the email address
  var name = emailAddress.split('@')[0].split('.');
  return name[0].substring(0, 1).toUpperCase() + name[0].substring(1) + ' ' +
         name[1].substring(0, 1).toUpperCase() + name[1].substring(1);
}



/**
 * Opens a window to select the date for the new practice.
 * @return {UiInstance} The created app with fields for dates
 */
function openAddPracticeInit() {
  var app = UiApp.createApplication().setTitle('Enter Date').setHeight(200);
  var grid = app.createGrid(1, 2);
  var newDW = app.createDateBox().setId('newDW');

  grid.setWidget(0, 0, app.createLabel('New practice date'));
  grid.setWidget(0, 1, newDW);

  var addButton = app.createButton('Add');
  var addPracticeHandler = app.createServerHandler('addPractice');
  addPracticeHandler.addCallbackElement(grid);
  addButton.addClickHandler(addPracticeHandler);

  var cancelButton = app.createButton('Cancel');
  var cancelHandler = app.createServerHandler('cancel');
  cancelButton.addClickHandler(cancelHandler);

  var panel = app.createVerticalPanel();
  panel.add(grid);
  panel.add(cancelButton);
  panel.add(addButton);
  app.add(panel);

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.show(app);

  return app;
}



/**
 * Adds a single practice at the date specified in the parameter.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function addPractice(e) {
  var d = new Date(e.parameter.newDW);
  var ui = SpreadsheetApp.getUi();

  if (e.parameter.newDW == '') {
    ui.alert('Invalid date given. Please try again.');
    throw new Error('addPractice(): Invalid date given');
  }

  if (d < new Date()) {
    ui.alert('Date has already passed. Please try again.');
    throw new Error('addPractice(): Date has already passed');
  }

  createPractice(d);
  var data = readFromSheet(SHEET_INDEX.PRACTICES);

  for (var i = 0; i < data.length; i++) {
    data[i][0] = new Date(data[i][0]);
  }

  var sortedData = data.sort(sortTwoDimensional);

  for (var i = 0; i < sortedData.length; i++) {
    sortedData[i][0] = sortedData[i][0].toString();
  }

  sortedData.reverse();
  writeToSheet(SHEET_INDEX.PRACTICES, sortedData);
  var scriptProperties = PropertiesService.getScriptProperties();
  var next = new Date(scriptProperties.getProperty('nextPractice'));

  //New practice is before the next practice
  if (d < next) {
    setNextPracticeDate(d.toString());
    setNextTriggers(d);
  }

  var app = UiApp.getActiveApplication();
  app.close();
  confirmComplete();

  return app;
}



/**
 * Opens a window to select the practice to delete.
 * @return {UiInstance} The created app with a list of selectable future dates
 */
function openRemovePracticeInit() {
  var app = UiApp.createApplication().setTitle('Select Date').setHeight(200);
  var panel = app.createVerticalPanel();
  var futurePractices = getFuturePractices();
  var futurePracticesWidget = app.createListBox().setId('futurePracticesWidget')
                              .setName('futurePracticesWidget');
  futurePracticesWidget.addItem('');

  for (var i = 0; i < futurePractices.length; i++) {
    futurePracticesWidget.addItem(futurePractices[i]);
  }

  var removeButton = app.createButton('Remove');
  var removePracticeHandler = app.createServerHandler('removePractice');
  removePracticeHandler.addCallbackElement(futurePracticesWidget);
  removeButton.addClickHandler(removePracticeHandler);

  var cancelButton = app.createButton('Cancel');
  var cancelHandler = app.createServerHandler('cancel');
  cancelButton.addClickHandler(cancelHandler);

  panel.add(futurePracticesWidget);
  panel.add(cancelButton);
  panel.add(removeButton);
  app.add(panel);

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.show(app);

  return app;
}



/**
 * Removes a single practice at the date specified in the parameter.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function removePractice(e) {
  var dateString = e.parameter.futurePracticesWidget;
  var ui = SpreadsheetApp.getUi();
  var remain = [];

  if (dateString == '') {
    ui.alert('No date given. Please try again.');
    throw new Error('removePractice(): No date given');
  }

  try {
    var data = readFromSheet(SHEET_INDEX.PRACTICES);
  }
  catch (error) {
    ui.alert('Practice sheet empty. Please try again.');
    throw new Error('removePractice(): Practice sheet empty');
  }

  if (dateString == getNextPracticeDate()) {
    advancePractice();
    setNextTriggers();
  }

  //Create a new array of every practice except the one being removed
  for (var i = 0; i < data.length; i++) {
    if (dateString != data[i][0]) {
      remain.push(data[i]);
    }
  }

  writeToSheet(SHEET_INDEX.PRACTICES, remain);

  var app = UiApp.getActiveApplication();
  app.close();
  confirmComplete();

  return app;
}



/**
 * Returns an array of all practices that have yet to occur as determined by
 * the nextPractice property.
 * @return {String[]} A 2D array of future practices
 */
function getFuturePractices() {
  var data = readFromSheet(SHEET_INDEX.PRACTICES);
  var next = getNextPracticeDate();
  var index = 0;

  for (var i = 0; i < data.length; i++) {
    if (data[i][0] == next) {
      index = i;
      break;
    }
  }

  return getCol(data.slice(index), 0);
}



/**
 * Opens a window to add a new team member.
 * @return {UiInstance} The created app with fields for a person's information
 */
function openAddTeamMemberInit() {
  var app = UiApp.createApplication().setTitle('Enter Team Member')
            .setWidth(700).setHeight(200);
  var grid = app.createGrid(2, 5);
  var headerArray = ['First Name', 'Last Name', 'Email', 'Car Capacity',
                     'Driver Willingness'];

  for (var i = 0; i < headerArray.length; i++) {
    grid.setWidget(0, i, app.createLabel(headerArray[i]));
  }

  var newDW = app.createDateBox().setId('email');
  var firstNameWidget = app.createTextBox().setId('firstName')
                        .setName('firstName');
  var lastNameWidget = app.createTextBox().setId('lastName')
                       .setName('lastName');
  var emailAddressWidget = app.createTextBox().setId('emailAddress')
                           .setName('emailAddress')
  var capacityWidget = app.createListBox().setId('capacity')
                       .setName('capacity');
  var willingnessWidget = app.createListBox().setId('willingness')
                          .setName('willingness');

  capacityWidget.addItem('');

  for (var i = 1; i <= 7; i++) {
    capacityWidget.addItem(''+i);
  }

  willingnessWidget.addItem('');

  for (var i = 1; i <= 10; i++) {
    willingnessWidget.addItem(''+i);
  }

  grid.setWidget(1, 0, firstNameWidget);
  grid.setWidget(1, 1, lastNameWidget);
  grid.setWidget(1, 2, emailAddressWidget);
  grid.setWidget(1, 3, capacityWidget);
  grid.setWidget(1, 4, willingnessWidget);

  var addButton = app.createButton('Add');
  var addPracticeHandler = app.createServerHandler('addTeamMember');
  addPracticeHandler.addCallbackElement(grid);
  addButton.addClickHandler(addPracticeHandler);

  var cancelButton = app.createButton('Cancel');
  var cancelHandler = app.createServerHandler('cancel');
  cancelButton.addClickHandler(cancelHandler);

  var panel = app.createVerticalPanel();
  panel.add(grid);
  panel.add(cancelButton);
  panel.add(addButton);
  app.add(panel);

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.show(app);

  return app;
}



/**
 * Adds a single team member as specified in the parameters.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function addTeamMember(e) {
  var data = readFromSheet(SHEET_INDEX.TEAM);
  data.push([e.parameter.firstName,
             e.parameter.lastName,
             e.parameter.emailAddress,
             e.parameter.capacity,
             e.parameter.willingness
            ]);

  //Do not check driver columns for missing info, since not every person drives
  if (confirmMissing(data, 3)) {
    writeToSheet(SHEET_INDEX.TEAM, data);
    confirmComplete();
  }

  var app = UiApp.getActiveApplication();
  app.close();

  return app;
}



/**
 * Opens a window to select the team member to delete.
 * @return {UiInstance} The created app with a list of selectable future dates
 */
function openRemoveTeamMemberInit() {
  var app = UiApp.createApplication().setTitle('Select Team Member')
            .setHeight(200);
  var panel = app.createVerticalPanel();
  var names = getTeamNames();
  var namesWidget = app.createListBox().setId('namesWidget')
                    .setName('namesWidget');
  namesWidget.addItem('');

  for (var i = 0; i < names.length; i++) {
    namesWidget.addItem(names[i]);
  }

  var removeButton = app.createButton('Remove');
  var removeTeamMemberHandler = app.createServerHandler('removeTeamMember');
  removeTeamMemberHandler.addCallbackElement(namesWidget);
  removeButton.addClickHandler(removeTeamMemberHandler);

  var cancelButton = app.createButton('Cancel');
  var cancelHandler = app.createServerHandler('cancel');
  cancelButton.addClickHandler(cancelHandler);

  panel.add(namesWidget);
  panel.add(cancelButton);
  panel.add(removeButton);
  app.add(panel);

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.show(app);

  return app;
}



/**
 * Removes a single team member as specified in the parameters.
 * @param {Object} e The parameters passed by the server handler
 * @return {UiInstance} The closed app
 */
function removeTeamMember(e) {
  var ui = SpreadsheetApp.getUi();
  var name = e.parameter.namesWidget;
  var firstName = name.split(' ')[0];
  var lastName = name.split(' ')[1];
  var remain = [];

  if (name == '') {
    ui.alert('No name given. Please try again.');
    throw new Error('removeTeamMember(): No name given');
  }

  try {
    var data = readFromSheet(SHEET_INDEX.TEAM);
  }
  catch (error) {
    ui.alert('Team sheet empty. Please try again.');
    throw new Error('removeTeamMember(): Team sheet empty');
  }

  //Create a new array of every team member except the one being removed
  for (var i = 0; i < data.length; i++) {
    if (!(firstName == data[i][0] && lastName == data[i][1])) {
      remain.push(data[i]);
    }
  }

  writeToSheet(SHEET_INDEX.TEAM, remain);

  var app = UiApp.getActiveApplication();
  app.close();
  confirmComplete();

  return app;
}



/**
 * Sets the triggers to send reminder emails and to close the form.
 * @param {Date} d The practice date for which to create triggers (optional)
 */
function setNextTriggers(d) {
  deleteTriggers();
  //Assigns d to a user-defined value if provided
  var d = typeof d !== 'undefined' ? d : new Date(getNextPracticeDate());
  //Day before practice at 1:00 PM
  var remindDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 1,
                            13, 0, 0, 0);
  //Day before practice at 8:00 PM
  var closeDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 1,
                           20, 0, 0, 0);
  setTrigger(remindDate, 'remind');
  setTrigger(closeDate, 'closeForm');

  return;
}



/**
 * Sends out reminder emails to those who have not yet filled out the form.
 */
function remind() {
  var pair = getNextPracticeIDs();
  var ssId = pair[0];
  var formId = pair[1];
  var ss = SpreadsheetApp.openById(ssId);
  var sheet = ss.getSheetByName('Form Responses 1');
  var data = sheet.getDataRange().getValues();
  var uniqueData = getUniqueSubmissions(data);
  var noResponse = getNonResponders(uniqueData);
  var formURL = FormApp.openById(formId).getPublishedUrl();

  for (var i = 0; i < noResponse.length; i++) {
    var emailAddress = noResponse[i];
    sendReminder(emailAddress, formURL);
  }

  return;
}



/**
 * Sends a reminder asking a person who has not responded to fill out the form.
 * @param {String} emailAddress The email address of the person being reminded
 * @param {String} formURL The URL of this week's form
 */
function sendReminder(emailAddress, formURL) {
  var body = 'This is an automated message. It appears that you have not yet '+
             'filled out the form for attendance this week. This week\'s form '+
             'can be found here:\n' + formURL + '\n\nIf you believe this is a '+
             'mistake, contact the captains immediately. Thank you for your '+
             'cooperation.';
  MailApp.sendEmail(emailAddress, 'Reminder about practice attendance', body);

  return;
}



function clearEverything() {
  deleteTriggers();
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(SHEET_INDEX.PRACTICES.name);
  sheet.clear();
  var scriptProperties = PropertiesService.getScriptProperties();
  Logger.log(scriptProperties.getProperties());
  scriptProperties.deleteAllProperties();

  return;
}



/**
 * Writes an array of data to the given sheet.
 * @param {Object} sheetId The object identifying the sheet to be written to
 * @param {Object[][]} data The data to be written to the given sheet
 */
function writeToSheet(sheetId, data) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(sheetId.name);

  //Sheet does not exist yet
  if (!sheet) {
    sheet = ss.insertSheet(sheetId.name, sheetId.index);
  }
  else {
    sheet.clear();
  }

  //Writing blank data, so exit immediately after clearing
  if (data.length == 0) {
    sheet.clear();
    return;
  }

  sheet.getRange(1, 1, data.length, data[0].length).setValues(data);

  return;
}



/**
 * Returns all data from the specified sheet as an array.
 * @param {Object} sheetId The object identifying the sheet to be written from
 * @return {Object[][]} A 2D array of the data from the sheet
 */
function readFromSheet(sheetId) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(sheetId.name);

  //Sheet does not exist yet
  if (!sheet) {
    throw new Error('readFromSheet(): sheet "'+sheetId.name+
                    '" does not exist');
  }

  //Sheet is empty
  if (sheet.getLastRow() == 0) {
    var ui = SpreadsheetApp.getUi();
    ui.alert('Sheet '+sheetId.name+' is empty');
    throw new Error('readFromSheet(): Sheet "'+sheetId.name+'" is empty');
  }

  return sheet.getRange(1, 1, sheet.getLastRow(), sheet.getLastColumn())
         .getValues();
}



/**
 * Returns the given date in string of the format MM/DD/YYYY.
 * @param {Date} d The date to be outputted as an American date string
 * @return {String} A string of the format MM/DD/YYYY
 */
function simpleDate(d) {
  var month =  d.getMonth();
  month = (month+1) < 10 ? "0" + (month+1) : (month+1);
  var date = d.getDate();
  date = date < 10 ? "0" + date : date;

  return month + '/' + date + '/' + d.getFullYear();
}



/**
 * Returns a column of a 2D array
 * @param {Object[][]} array The array from which the column will be pulled
 * @param {Integer} col The column to be selected
 * @return {Object[]} An array with the data from the selected column
 */
function getCol(array, col) {
  var column = [];

  for (var i = 0; i < array.length; i++) {
    column.push(array[i][col]);
  }

  return column;
}



/**
 * Sorts a two dimensional array by the FIRST element.
 */
function sortTwoDimensional(a, b) {
  return b[0] - a[0];
}



/**
 * Sorts a multidimensional array by the THIRD element.
 */
function sortMultiDimensional(a, b) {
  return b[2] - a[2];
}



/**
 * Asks for confirmation to write if the data is incomplete
 * @param {Object[][]} data A 2D array of the data to check
 * @param {Integer} maxCol The maximum column to check (optional)
 * @return {Boolean} Whether the data should be written
 */
function confirmMissing(data, maxCol) {
  //Assigns maxCol to a user-defined value if provided
  maxCol = typeof maxCol !== 'undefined' ? maxCol : data[0].length;
  var missingInfo = false;

  //Checks all rows and columns for blank fields
  for (var i = 0; i < data.length; i++) {
    for (var j = 0; j < maxCol; j++) {
      if (data[i][j] == '') {
        missingInfo = true;
        break;
      }
    }
  }

  if (missingInfo) {
    var promptString = 'There is missing information. Are you sure you want to'+
                       ' sumbit this data?';
    var ui = SpreadsheetApp.getUi();
    var response = ui.alert(promptString, ui.ButtonSet.YES_NO);

    if (response == ui.Button.YES) {
      return true;
    }
  }
  else {
    return true;
  }

  return false;
}



/**
 * Shows a confirmation toast pop-up and deletes the default sheet if it exists.
 */
function confirmComplete() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.toast('Operation successfully completed', 'Success', 60);
  var defaultSheet = ss.getSheetByName('Sheet1');

  if (defaultSheet) {
    ss.deleteSheet(defaultSheet);
  }

  return;
}



/**
 * Draws the menu which contains all Skeet & Trap functions.
 */
function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Skeet & Trap')
    .addItem('Enter break dates', 'openDateInit')
    .addItem('Enter leadership information', 'leadershipInit')
    .addItem('Enter team information', 'teamInit')
    .addItem('Initialize calendar', 'openCalendarInit')
    .addSeparator()
    .addItem('Add a practice', 'openAddPracticeInit')
    .addItem('Remove a practice', 'openRemovePracticeInit')
    .addSeparator()
    .addItem('Add a team member', 'openAddTeamMemberInit')
    .addItem('Remove a team member', 'openRemoveTeamMemberInit')
    .addToUi();
}
